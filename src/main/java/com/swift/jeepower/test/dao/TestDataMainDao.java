/**
 * LD
 */
package com.swift.jeepower.test.dao;

import com.swift.jeepower.common.persistence.CrudDao;
import com.swift.jeepower.common.persistence.annotation.MyBatisDao;
import com.swift.jeepower.test.entity.TestDataMain;

/**
 * 主子表生成DAO接口
 * @author ThinkGem
 * @version 2015-04-06
 */
@MyBatisDao
public interface TestDataMainDao extends CrudDao<TestDataMain> {
	
}