/**
 * 
 */
package com.swift.jeepower.modules.cms.dao;

import com.swift.jeepower.common.persistence.CrudDao;
import com.swift.jeepower.common.persistence.annotation.MyBatisDao;
import com.swift.jeepower.modules.cms.entity.ArticleData;

/**
 * 文章DAO接口
 * @author ThinkGem
 * @version 2013-8-23
 */
@MyBatisDao
public interface ArticleDataDao extends CrudDao<ArticleData> {
	
}
