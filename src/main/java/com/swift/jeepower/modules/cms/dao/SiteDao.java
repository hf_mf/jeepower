/**
 * 
 */
package com.swift.jeepower.modules.cms.dao;

import com.swift.jeepower.common.persistence.CrudDao;
import com.swift.jeepower.common.persistence.annotation.MyBatisDao;
import com.swift.jeepower.modules.cms.entity.Site;

/**
 * 站点DAO接口
 * @author ThinkGem
 * @version 2013-8-23
 */
@MyBatisDao
public interface SiteDao extends CrudDao<Site> {

}
