/**
 * 
 */
package com.swift.jeepower.modules.act.dao;

import com.swift.jeepower.common.persistence.CrudDao;
import com.swift.jeepower.common.persistence.annotation.MyBatisDao;
import com.swift.jeepower.modules.act.entity.Act;

/**
 * 审批DAO接口
 * @author thinkgem
 * @version 2014-05-16
 */
@MyBatisDao
public interface ActDao extends CrudDao<Act> {

	public int updateProcInsIdByBusinessId(Act act);
	
}
