/**
 * LD
 */
package com.swift.jeepower.modules.sys.dao;

import com.swift.jeepower.common.persistence.TreeDao;
import com.swift.jeepower.common.persistence.annotation.MyBatisDao;
import com.swift.jeepower.modules.sys.entity.Area;

/**
 * 区域DAO接口
 * @author ThinkGem
 * @version 2014-05-16
 */
@MyBatisDao
public interface AreaDao extends TreeDao<Area> {
	
}
